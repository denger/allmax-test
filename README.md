Тестовое задание для компании Allmax
========

Rest-backend на Django для TODO-списка.

Example
=======

Из консоли можно проверить с помощью CURL.

Registration
------------

curl -X POST -d "username=tester&password=password123" http://localhost:8000/api/registration/

Get JWT token (login)
----

curl -X POST -d "username=tester&password=password123" http://localhost:8000/api/token-auth/

Create new task
----

curl -X POST -H "Content-Type: application/json" -H "Authorization: JWT <your_token>"  -d '{"title": "test task"}' http://localhost:8000/api/tasks/

Get tasks
----

curl -H "Authorization: JWT <your_token>" http://localhost:8000/api/tasks/

Get tasks with filters
----

curl -H "Authorization: JWT <your_token>" http://localhost:8000/api/tasks/\?priority=5&completed=false


Get one task
----

curl -H "Authorization: JWT <your_token>" http://localhost:8000/api/tasks/42/


PUT и PATCH также работают.

