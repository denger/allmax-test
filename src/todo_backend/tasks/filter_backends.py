from rest_framework.filters import BaseFilterBackend


class PriorityFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        qs = queryset._clone()
        priority = request.query_params.get('priority')
        if priority is None:
            return qs

        qs = qs.filter(priority=priority)
        return qs


class CompletedFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        qs = queryset._clone()
        completed = request.query_params.get('completed')
        if completed is None:
            return qs

        if completed == 'true':
            qs = qs.filter(completed=True)
        elif completed == 'false':
            qs = qs.filter(completed=False)
        return qs

