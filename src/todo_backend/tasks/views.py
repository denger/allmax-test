from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import Task
from .serializers import TaskSerializer
from .filter_backends import PriorityFilterBackend, CompletedFilterBackend


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = TaskSerializer
    queryset = Task.objects.all()
    permission_classes = (IsAuthenticated,)
    filter_backends = (PriorityFilterBackend, CompletedFilterBackend)

    def filter_queryset(self, queryset):
        qs = queryset._clone()
        if not self.request.user.groups.filter(name='admin').exists():
            qs = queryset.filter(user=self.request.user)
        return super(TaskViewSet, self).filter_queryset(qs)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

