from django.db import models
from django.core import validators


class Task(models.Model):
    title = models.CharField(max_length=128)
    description = models.TextField(default='', null=False, blank=True)
    completed = models.BooleanField(default=False)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    priority = models.PositiveSmallIntegerField(
            validators=[validators.MinValueValidator(1),
                validators.MaxValueValidator(10)],
            default=5)

    class Meta:
        ordering = '-completed', '-priority',

    def __str__(self):
        return self.title

