from django.contrib import admin
from .models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = 'title', 'user', 'completed'
    list_filter = 'completed',
    raw_id_fields = 'user',

